from disk import *

class Check:

    def __init__(self, args):
        self.args       = args
        self.arguments  = {}

    def check_directories(self):

        for idx, path in enumerate(self.args['--destination']):
            if path[-1:] == "/":
                self.args['--destination'][idx] = disk(self.args['--destination'][idx][:-1])

        if self.args['--source'][-1:] == "/":
                self.args['--source'] = self.args['--source'][:-1]

        self.arguments['destinations']  = self.args['--destination']
        self.arguments['source']        = self.args['--source']

    def check_threads(self):

        if int(self.args['--threads']) <= 0:
            raise ValueError("Number of threads is too low (" + self.args['--threads'] + ")")
        elif int(self.args['--threads']) > 64:
            raise ValueError("Number of threads is too high (" + self.args['--threads'] + ")")
        else:
            self.arguments['threads'] = int(self.args['--threads'])

    def check_integrity(self):
        self.arguments['integrity-check'] = not self.args['--no-integrity-check']

    def enable_verbose(self):
        return self.args['--verbose']
