from os import path

import utils
import logging
from utils import Color


class File:

    def __init__(self, name, src, dst):
        self.name          = name
        self.source        = src
        self.destination   = dst

    def check_integrity(self):
        logging.debug('Checking file integrity for %s', self.name)
        if  path.isfile(self.source) is True and \
            path.isfile(self.destination) is True and \
            utils.crc(self.source) != utils.crc(self.destination):

            logging.warning('%s is not the same at source (%s) and destination (%s)', self.name, Color.GREEN + self.source + Color.RESET, Color.RED + self.destination + Color.RESET)

        return

    def size(self):
        return path.getsize(self.source)