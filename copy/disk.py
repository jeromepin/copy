import logging
import os
import json

class disk(object):
    def __init__(self, path):
        logging.debug('adding new disk')
        self.path               = path
        self.status_file        = self.path + '/.splitit'
        self.status_file_loaded = False
        self.status             = None
#        self.create_dotfile_if_not_exist('splitit')

    def create_status_file_if_not_exist(self, filename):
        self.status_file = self.path + "/." + filename

        with open(self.status_file, "a+") as f:
            f.write('')

    def can_store(self, file):
        if file.size() <= self.get_free_space():
            ret = True
        else:
            ret = False
            logging.warning("Not enough free space left on %s to copy %s", self.path, file.name)

        return ret

    def has_already_copied(self, filename):
        if os.path.isfile(self.status_file) and self.status is None:
            self.get_status_from_file()

        if self.status is not None:
            return (filename in self.status['all_files'] or filename in self.status[self.path])

        return False

    def write_status_file(self, content):
        with open(self.status_file, "w") as outfile:
            json.dump(content, outfile, indent=2)

    def get_free_space(self):
        statvfs = os.statvfs(self.path)
        return statvfs.f_frsize * statvfs.f_bavail

    def get_status_from_file(self):
        with open(self.status_file, 'r') as f:
            self.status = json.load(f)

        return self.status
