import hashlib
import zlib
import os
import subprocess
import logging

def md5(file):
    md5 = hashlib.md5()
    with open(file,'rb') as f:
        for chunk in iter(lambda: f.read(8192), b''):
            md5.update(chunk)

    return md5.digest()

def crc(file):
    prev = 0
    for line in open(file,"rb"):
        prev = zlib.crc32(line, prev)
    return "%X"%(prev & 0xFFFFFFFF)


class Color:
    HEADER  = '\033[95m'
    BLUE    = '\033[94m'
    GREEN   = '\033[92m'
    YELLOW  = '\033[93m'
    RED     = '\033[91m'
    RESET   = '\033[0m'


