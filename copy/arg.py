# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from docopt     import docopt

help = """co.py

Usage:
  co [-n] [-v] [-t THREADS] -s SOURCE -d DESTINATION...

Options:
  -s, --source SOURCE               Source directory
  -d, --destination DESTINATION     Destination directory

  -n, --no-integrity-check          Disable (CRC) integrity checking after copy
  -t, --threads THREADS             Number of threads to use [default: 2]
  -v, --verbose                     Verbose

  -h, --help                        Print help menu
  --version                         Display program version
"""

def parse():
    return docopt(help, version='0.0.5')
