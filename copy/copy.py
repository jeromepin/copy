#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import arg
import shutil
import logging
import json
import signal
import sys

from check import *
from file import *
from threadpool import ThreadPool


class Copy:

    def __init__(self, args):
        self._source            = args['source']
        self._destinations      = args['destinations']
        self._threads           = args['threads']
        self._integrity_check   = args['integrity-check']

        self._destination_id    = -1
        self._common_path       = self._source
        self._soft_root         = ""
        self._files             = []
        self._pool              = ThreadPool(self._threads)

        if os.path.exists(self._destinations[0].path + '/.splitit'):
            self._status = self._destinations[0].get_status_from_file()
        else:
            self._status = {}
            self._status['all_files'] = []
            for dst in self._destinations:
                self._status[dst.path] = []

        print(self._status)

    def run(self):
        logging.info('Copying files...')
        logging.debug('Copying from %s to ', self._source)
        
        for dst in self._destinations:
            logging.debug('-> %s', dst.path)

        for (root, directories, files) in os.walk(self._source):

            self._soft_root = root[len(self._source):]

            # If it is a folder
            for directory in directories:
                # Create the folder in each destinations
                for dst in self._destinations:
                    path = dst.path + self._soft_root + '/' + directory

                    if not os.path.exists(path):
                        logging.debug('Creating new directory %s in %s', directory, path)
                        os.makedirs(path)

            # If it is a file
            for raw_file in files:
                disk    = self._destinations[self.choose_destination()]
                file    = File(raw_file, root + "/" + raw_file, disk.path + self._soft_root + "/" + raw_file)

                if disk.can_store(file) and not disk.has_already_copied(file.destination):
                    self._files.append(file)
                    self._pool.add_task(self.copy, file, disk)
 
            for sig in (signal.SIGINT, signal.SIGTERM):
                signal.signal(sig, self.finish)

        self._pool.destroy()
        if self._integrity_check:
            self.check_files_integrity()

        self.finish()

    def choose_destination(self):
        if self._destination_id < len(self._destinations) - 1:
            self._destination_id += 1
        else:
            self._destination_id = 0

        logging.debug('Next destination is %s', self._destinations[self._destination_id].path)
        return self._destination_id

    def copy(self, file, disk):
        logging.debug("Copying %s to %s", file.name, file.destination)
        shutil.copy2(file.source, file.destination)
        
        self._status['all_files'].append(file.destination)
        self._status[disk.path].append(file.destination)

        return

    def check_files_integrity(self):
        logging.info("Checking files integrity...")
        check_integrity_pool = ThreadPool(self._threads)
        for file in self._files:
            check_integrity_pool.add_task(file.check_integrity)

        check_integrity_pool.destroy()

    def finish(self, *args):
        self.status = json.dumps(self._status)
        for dst in self._destinations:
            dst.write_status_file(self._status);
        
        print(self._status)
        sys.exit(0)


if __name__ == '__main__':

    logger  = logging.getLogger()
    logger.setLevel(logging.INFO)
    logging.basicConfig(format='%(levelname)s :: %(message)s');
    
    checker = Check(arg.parse())
    
    try:
        checker.check_directories()
        checker.check_threads()
        checker.check_integrity()

        if checker.enable_verbose() is True:
            logger.setLevel(logging.DEBUG)

        copy = Copy(checker.arguments)
        copy.run()
    except Exception as exception:
        logging.error(exception)
