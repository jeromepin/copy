from distutils.core import setup

setup(
    name = "copy",
    packages = ["copy"],
    version = "0.0.5",
    description = "Multiple destination file copy",
    author = "Jerome Pin",
    author_email = "jeromepin38@gmail.com",
    url = "https://gitlab.com/jeromepin/copy",
    download_url = "https://gitlab.com/jeromepin/copy/repository/archive.tar.gz?ref=v0.0.5",
    keywords = ["file", "copy"],
    classifiers = [
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Natural Language :: English",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
    ],
)