# co.py

# **WARNING** : This is a beta release, official release with Pypi packaging comes soon

## Installation


First you need to install dependencies :

```bash
	pip install docopt
```

Then clone the latest development version from git :

```bash
	git clone https://gitlab.com/jeromepin/copy.git
	cd copy/copy
```


## Usage

	Usage:
		co [-n] [-v] [-t THREADS] [-a ALGORITHM] -s SOURCE -d DESTINATION...

	Options:
		-s, --source SOURCE               Source directory
		-d, --destination DESTINATION     Destination directory

		-a, --algorithm ALGORITHM         Algorithm to use for destination choice [default: ROUND_ROBIN]
		-n, --no-integrity-check          Disable (CRC) integrity checking after copy
		-t, --threads THREADS             Number of threads to use [default: 4]
		-v, --verbose                     Verbose

		-h, --help                        Print help menu
		--version                         Display program version


## Examples

```bash
    python2.7 ./copy -s /tmp/src -d /mnt/disk1 -d /mnt/disk2
```
